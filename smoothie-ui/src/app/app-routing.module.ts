import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SmoothieListComponent } from './pages/smothie/components/smoothie-list/smoothie-list.component';
import { SmoothieDetailsComponent } from './pages/smothie/components/smoothie-details/smoothie-details.component';
import { LoginComponent } from './pages/authorization/components/login/login.component';
import { AuthGuard } from './pages/authorization/service/auth.guard';
import { OrderComponent } from './pages/orders/components/order/order.component';
import { OrderSummaryComponent } from './pages/orders/components/order-summary/order-summary.component';
import { RegisterComponent } from './pages/authorization/components/register/register.component';

const routes: Routes = [
  { path: '', redirectTo: 'smoothies', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'smoothies', component: SmoothieListComponent, canActivate: [AuthGuard] },
  { path: 'smoothies/details/:id', component: SmoothieDetailsComponent, canActivate: [AuthGuard] },
  { path: 'order', component: OrderComponent, canActivate: [AuthGuard] },
  { path: 'order/:id', component: OrderSummaryComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
