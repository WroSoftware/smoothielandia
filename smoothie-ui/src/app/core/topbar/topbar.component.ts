import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/pages/authorization/service/authorization.service';


@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {


  constructor( 
    private router: Router,
    private authorizationService: AuthorizationService
    ) { }

  ngOnInit(): void {
  }

  get logged() {
    return this.authorizationService.getToken() != null;
  }

  get login() {
    return this.authorizationService.getUserName();
  }

  get role() {
    return this.authorizationService.getRole();
  }
  

  onLogout() {
    localStorage.removeItem('access_token')
    this.router.navigate(['login']);
  }

}
