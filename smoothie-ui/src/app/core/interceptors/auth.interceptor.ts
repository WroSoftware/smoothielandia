import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthorizationService } from 'src/app/pages/authorization/service/authorization.service';

const TOKEN_HEADER_KEY = "Authorization";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authorizationService: AuthorizationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.authorizationService.getToken();
    let authRequest = token != null
      ? request.clone({headers: request.headers.set(TOKEN_HEADER_KEY, `Bearer ${token}`)})
      : request;
    return next.handle(authRequest);
  }
}
