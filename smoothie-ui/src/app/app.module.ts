import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './core/app/app.component';
import { CoreModule } from './core/core.module';
import { SmothieModule } from './pages/smothie/smothie.module';
import { AuthorizationModule } from './pages/authorization/authorization.module';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { OrdersModule } from './pages/orders/orders.module';

@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    AuthorizationModule,
    SmothieModule,
    OrdersModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
