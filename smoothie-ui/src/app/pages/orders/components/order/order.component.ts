import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Smoothie } from 'src/app/pages/smothie/model/smoothie';
import { SmoothieService } from 'src/app/pages/smothie/service/smoothie.service';
import { OrderService } from '../../service/order.service';
import { Router } from '@angular/router';
import { Order } from '../../model/order';
import { OrderItem } from '../../model/order-item';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  smoothieData = new MatTableDataSource<Smoothie>([]);
  itemsData = new MatTableDataSource<OrderItem>([]);
  smoothieDisplayedColumns = ['name', 'price', 'actions'];
  itemsDisplayedColumns = ['name', 'quantity', 'actions'];
  order: Order = {
    items: [],
    totalPrice: 0
  };
  currency!: string;

  constructor(
    private smoothieService: SmoothieService,
    private orderService: OrderService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.smoothieService.getSmoothies().subscribe(response => {
      this.smoothieData.data = response
      this.currency = response[0]?.currency;
    });
    this.itemsData.data = this.order.items;
  }

  addSmoothie(smoothiie: Smoothie) {
    let item = this._getItemFromOrder(smoothiie);
    this.increaseQuantity(item);
  }

  increaseQuantity(item: OrderItem) {
    item.quantity = item.quantity + 1;
    this.order.totalPrice = this.order.totalPrice + item.smoothie.price;
    this.itemsData.data = this.order.items;
  }

  decreaseQuantity(item: OrderItem) {
    if(item.quantity === 1) {
      this._removeItemFromOrder(item);
    }
    item.quantity = item.quantity - 1;
    this.order.totalPrice = this.order.totalPrice - item.smoothie.price;
    this.itemsData.data = this.order.items;
  }

  get anyToSave() {
    return this.order.items.length > 0;
  }

  onSave() {
    this.orderService.saveNewOrder(this.order).subscribe((response) => {
      this.router.navigate(["order", response.id])
    })
  }

  private _getItemFromOrder(smoothie: Smoothie): OrderItem {
    let items = this.order.items;
    let item = items.filter(item => item.smoothie.id == smoothie.id)[0];
    if(!item) {
      item = {smoothie: smoothie, quantity: 0};
      items.push(item);
    }
    return item; 
  }

  private _removeItemFromOrder(item: OrderItem) {
    let items = this.order.items;
    let index = items.indexOf(item);
    items.splice(index, 1);
  }
  
}
