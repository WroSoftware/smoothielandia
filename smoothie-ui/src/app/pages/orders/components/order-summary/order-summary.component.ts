import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { OrderItem } from '../../model/order-item';
import { OrderService } from '../../service/order.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Order } from '../../model/order';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit, OnDestroy {

  private subscription!: Subscription;
  order: any = {};
  currency = '';
  itemsData = new MatTableDataSource<OrderItem>([]);
  itemsDisplayedColumns = ['name', 'quantity']

  constructor(
    private orderService: OrderService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.subscription = this.activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      this.orderService.getOrderById(id).subscribe(order => {
        this.order = order;
        this.currency  = order.items[0]?.smoothie.currency;
        this.itemsData.data = order.items;
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
