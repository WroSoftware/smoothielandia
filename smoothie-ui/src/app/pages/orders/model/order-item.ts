import { Smoothie } from "../../smothie/model/smoothie";

export class OrderItem {

    constructor(
        public smoothie: Smoothie,
        public quantity: number
    ) {}
}
