import { OrderItem } from "./order-item";

export class Order {

    constructor(
        public items: OrderItem[],
        public totalPrice: number,
        public id?: string
    ) {}
}
