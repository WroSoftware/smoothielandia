import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SmoothieListComponent } from './components/smoothie-list/smoothie-list.component';
import { HttpClientModule } from '@angular/common/http';
import { SmoothieDialogComponent } from './components/smoothie-dialog/smoothie-dialog.component';
import { SmoothieDetailsComponent } from './components/smoothie-details/smoothie-details.component';
import { CoreModule } from 'src/app/core/core.module';



@NgModule({
  declarations: [
    SmoothieListComponent,
    SmoothieDialogComponent,
    SmoothieDetailsComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule
  ],
  exports: [
    SmoothieListComponent,
    SmoothieDetailsComponent
  ]
})
export class SmothieModule { }
