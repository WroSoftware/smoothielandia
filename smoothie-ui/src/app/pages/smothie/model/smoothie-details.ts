import { Nutrition } from "./nutrition";

export class SmoothieDetails {
    constructor(
       
        public name: string,
        public description: string,
        public price: number,
        public currency: string,
        public portionSize: number,
        public calories: number,
        public nutritions: Nutrition[],
        public id?: string
    ) {} 
}
