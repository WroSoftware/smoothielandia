export class Smoothie {
    
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public price: number,
        public currency: string
    ) {}
}
