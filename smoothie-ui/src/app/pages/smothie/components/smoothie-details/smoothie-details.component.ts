import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { SmoothieService } from '../../service/smoothie.service';
import { SmoothieDetails } from '../../model/smoothie-details';
import { SmoothieDialogComponent } from '../smoothie-dialog/smoothie-dialog.component';
import { Nutrition } from '../../model/nutrition';
import { AuthorizationService } from 'src/app/pages/authorization/service/authorization.service';

@Component({
  selector: 'app-smoothie-details',
  templateUrl: './smoothie-details.component.html',
  styleUrls: ['./smoothie-details.component.scss']
})
export class SmoothieDetailsComponent implements OnInit, OnDestroy {

  private subscription!: Subscription;
  smoothie!: SmoothieDetails;
  nutritionData = new MatTableDataSource<Nutrition>([]);
  displayedColumns = ['name', 'masss', 'percentage'];

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authorizationService: AuthorizationService,
    private smoothieService: SmoothieService
  ) {
    this.subscription = activatedRoute.params.subscribe((param: any) => {
      let id = param['id'];
      this.smoothieService.findSmoothieDetails(id).subscribe(response => {
        this.smoothie = response;
        this.nutritionData.data = response.nutritions;
      });
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  get canEdit() {
    return this.authorizationService.getRole() === 'BUSINESS_OWNER';
  }

  editSmoothie() {
    let dialogRef = this.dialog.open(SmoothieDialogComponent, {
      data: this.smoothie,
      height: '500px',
      width: '700px',
    });
    dialogRef.afterClosed().subscribe(() => this._reloadData());
  }

  backToSmoothieList() {
    this.router.navigate(['smoothies']);
  }

  private _reloadData() {
    const id = this.smoothie.id;
    if (id) {
      this.smoothieService.findSmoothieDetails(id).subscribe(response => {
        this.smoothie = response;
        this.nutritionData.data = this.smoothie.nutritions;
      });
    }
  }

}
