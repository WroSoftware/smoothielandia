import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Smoothie } from '../../model/smoothie';
import { SmoothieService } from '../../service/smoothie.service';
import { SmoothieDialogComponent } from '../smoothie-dialog/smoothie-dialog.component';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/pages/authorization/service/authorization.service';

@Component({
  selector: 'app-smoothie-list',
  templateUrl: './smoothie-list.component.html',
  styleUrls: ['./smoothie-list.component.scss']
})
export class SmoothieListComponent implements OnInit {

  smoothieData = new MatTableDataSource<Smoothie>([]);
  displayedColumns = ['name', 'price', 'actions'];

  constructor(
    public dialog: MatDialog,
    private smoothieService: SmoothieService,
    private authorizationService: AuthorizationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.smoothieService.getSmoothies().subscribe(response => this.smoothieData.data = response);
  }

  get canEdit() {
    return this.authorizationService.getRole() === 'BUSINESS_OWNER';
  }

  openNewSmoothieDialog() {
    let dialogRef = this.dialog.open(SmoothieDialogComponent, {
      data: {},
      height: '500px',
      width: '700px',
    });
    dialogRef.afterClosed().subscribe(() => {
      this.smoothieService.getSmoothies().subscribe(response => this.smoothieData.data = response);
    });
  }

  showDetails(smoothieId: string) {
    this.router.navigate(['smoothies','details',smoothieId])
  }

  edit(smoothieId: string) {
    this.smoothieService.findSmoothieDetails(smoothieId).subscribe(response => {
      let dialogRef = this.dialog.open(SmoothieDialogComponent, {
        data: response,
        height: '500px',
        width: '700px',
      });
      dialogRef.afterClosed().subscribe(() => {
        this.smoothieService.getSmoothies().subscribe(response => this.smoothieData.data = response);
      });
    });
  }

  delete(smoothieId: string) {
    this.smoothieService.deleteSmoothie(smoothieId).subscribe(() => {
      this.smoothieService.getSmoothies().subscribe(response => this.smoothieData.data = response);
    });
  }

}
