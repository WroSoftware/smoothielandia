import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Nutrition } from '../../model/nutrition';
import { SmoothieDetails } from '../../model/smoothie-details';
import { SmoothieService } from '../../service/smoothie.service';

@Component({
  selector: 'app-smoothie-dialog',
  templateUrl: './smoothie-dialog.component.html',
  styleUrls: ['./smoothie-dialog.component.scss']
})
export class SmoothieDialogComponent implements OnInit {

  smoothieForm!: FormGroup;
  nutritionForm!: FormGroup;
  availableNutritionTypes = ["Protein", "Fat", "Sugar", "Sodium", "Vitamin D", "Iron", "Potassium"]
  nutritionTypes: string[] = [];
  nutritions: Nutrition[] = [];
  newNutritionProcess = false;

  constructor(
    private smoothieService: SmoothieService,
    public dialogRef: MatDialogRef<SmoothieDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SmoothieDetails,
  ) { }

  ngOnInit(): void {
    if (this.data.id) {
      this._createFormGroupForEdition(this.data);
      this._createNutritionFormGroup();
      this.nutritions = this.data.nutritions;
    } else {
      this._createFormGroup();
      this._createNutritionFormGroup();
    }

  }

  removeNutrition(nutrition: Nutrition) {
    let index = this.nutritions.findIndex(item => item.name == nutrition.name);
    this.nutritions.splice(index, 1);
    this.nutritionTypes.push(nutrition.name);
  }

  newNutrition() {
    this.newNutritionProcess = true;
    this.nutritionTypes = this.availableNutritionTypes;
    this.nutritionForm.get('nutName')?.setValue('');
    this.nutritionForm.get('nutMass')?.setValue('');
    this.nutritionForm.get('nutPercentage')?.setValue('');
  }

  onAddNutrition() {
    let nutrition = {
      name: this.nutritionForm.value['nutName'],
      masss: this.nutritionForm.value['nutMass'],
      percentage: this.nutritionForm.value['nutPercentage'],
    };
    this.nutritions.push(nutrition);
    this.nutritionTypes.splice(this.nutritionTypes.findIndex(item => item == nutrition.name), 1)
    this.newNutritionProcess = false;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSave() {
    let smoothie = this._prepareSmoothie();
    if(this.data.id) {
      this.smoothieService.editSmoothie(smoothie).subscribe(() => this.dialogRef.close());
    } else {
      this.smoothieService.saveNewSmoothie(smoothie).subscribe(() => this.dialogRef.close());
    }
  }

  private _createFormGroup() {
    this.smoothieForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl(''),
      price: new FormControl('', Validators.required),
      currency: new FormControl({ value: 'USD', disabled: true }),
      portionSize: new FormControl(''),
      calories: new FormControl(''),
    });
  }

  private _createFormGroupForEdition(smoothie: SmoothieDetails) {
    this.smoothieForm = new FormGroup({
      name: new FormControl(smoothie.name, Validators.required),
      description: new FormControl(smoothie.description),
      price: new FormControl(smoothie.price, Validators.required),
      currency: new FormControl({ value: 'USD', disabled: true }),
      portionSize: new FormControl(smoothie.portionSize),
      calories: new FormControl(smoothie.calories),
    });
  }

  private _createNutritionFormGroup() {
    this.nutritionForm = new FormGroup({
      nutName: new FormControl(''),
      nutMass: new FormControl(''),
      nutPercentage: new FormControl(''),
    });
  }

  private _prepareSmoothie(): SmoothieDetails {
    return {
      name: this.smoothieForm.value['name'],
      description: this.smoothieForm.value['description'],
      price: this.smoothieForm.value['price'],
      currency: 'USD',
      portionSize: this.smoothieForm.value['portionSize'],
      calories: this.smoothieForm.value['calories'],
      nutritions: this.nutritions,
      id: this.data.id
    }
  }
}
