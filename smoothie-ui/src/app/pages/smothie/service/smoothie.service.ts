import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Smoothie } from '../model/smoothie';
import { Observable } from 'rxjs';
import { SmoothieDetails } from '../model/smoothie-details';

@Injectable({
  providedIn: 'root'
})
export class SmoothieService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getSmoothies(): Observable<Smoothie[]> {
    return this.http.get<Smoothie[]>(`${this.baseUrl}/smoothies`);
  }

  saveNewSmoothie(smoothie: SmoothieDetails): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/smoothies`, smoothie);
  }

  editSmoothie(smoothie: SmoothieDetails): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/smoothies`, smoothie);
  }

  deleteSmoothie(smoothieId: string): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/smoothies/${smoothieId}`);
  }

  findSmoothieDetails(smoothieId: string): Observable<SmoothieDetails> {
    return this.http.get<SmoothieDetails>(`${this.baseUrl}/smoothies/${smoothieId}`);
  }


}
