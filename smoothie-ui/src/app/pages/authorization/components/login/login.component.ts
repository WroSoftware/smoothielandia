import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthorizationModule } from '../../authorization.module';
import { AuthorizationService } from '../../service/authorization.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(
    private router: Router,
    private authorizationService: AuthorizationService
  ) { }

  ngOnInit(): void {
    this._createForm();
  }

  onLogin() {
    this.authorizationService.login(this.loginForm.value).subscribe((response) => {
      localStorage.setItem('access_token', response.token);
      this.router.navigate(['smoothies']);
    });
  }

  onRegister() {
    this.router.navigate(['register']);
  }

  private _createForm() {
    this.loginForm = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

}
