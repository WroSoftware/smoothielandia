import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../service/authorization.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm!: FormGroup;

  constructor(
    private router: Router,
    private authorizationService: AuthorizationService
  ) { }

  ngOnInit(): void {
    this._createForm();
  }

  onRegister() {
    this.authorizationService.register(this.registerForm.value).subscribe((response) => {
      localStorage.setItem('access_token', response.token);
      this.router.navigate(['smoothies']);
    });
  }

  private _createForm() {
    this.registerForm = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      userRole: new FormControl('USER', Validators.required),
    });
  }

}
