import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private baseUrl = environment.baseUrl;


  constructor(private http: HttpClient) { }

  login(authData: {login: string, password: string}): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/login`, authData);
  } 

  register(registerData: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/register`, registerData);
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  getUserName() {
    return this._getTokenInfo().sub;
  }

  getRole() {
    return this._getTokenInfo().role;
  }

  get isLoggedIn(): boolean {
    return this.getToken() !== null ? true : false;
  }



  private _getTokenInfo(): any {
    const token = this.getToken();
    return token? jwt_decode(token): {};
  }

}
