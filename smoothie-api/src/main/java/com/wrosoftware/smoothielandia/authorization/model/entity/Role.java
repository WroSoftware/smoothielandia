package com.wrosoftware.smoothielandia.authorization.model.entity;

public enum Role {
    BUSINESS_OWNER,
    USER
}
