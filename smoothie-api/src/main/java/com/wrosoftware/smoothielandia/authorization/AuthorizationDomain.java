package com.wrosoftware.smoothielandia.authorization;

import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationResponseDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.RegisterDto;

public interface AuthorizationDomain {

    AuthorizationResponseDto login(AuthorizationDto dto);

    AuthorizationResponseDto register(RegisterDto dto);
}
