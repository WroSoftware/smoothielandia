package com.wrosoftware.smoothielandia.authorization;

import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationResponseDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.RegisterDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
class AuthorizationController {

    private final AuthorizationDomain authorizationDomain;

    @PostMapping("/login")
    public AuthorizationResponseDto login(@RequestBody AuthorizationDto dto){
        return authorizationDomain.login(dto);
    }

    @PostMapping("/register")
    public AuthorizationResponseDto register(@RequestBody RegisterDto dto) {
        return authorizationDomain.register(dto);
    }
}
