package com.wrosoftware.smoothielandia.authorization.domain;

import com.wrosoftware.smoothielandia.authorization.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String login);
}
