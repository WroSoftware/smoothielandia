package com.wrosoftware.smoothielandia.authorization.model.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RegisterDto {

    private String login;
    private String password;
    private String userRole;

}
