package com.wrosoftware.smoothielandia.authorization.domain;

import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.RegisterDto;
import com.wrosoftware.smoothielandia.authorization.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
interface UserMapper {

    @Mapping(target = "role", source = "dto.userRole")
    @Mapping(target = "password", source = "encodedPassword")
    User mapToUser(RegisterDto dto, String encodedPassword);

    AuthorizationDto mapToAuthorizationDto(User user);
}
