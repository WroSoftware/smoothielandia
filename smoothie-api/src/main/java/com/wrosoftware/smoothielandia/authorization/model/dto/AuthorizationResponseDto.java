package com.wrosoftware.smoothielandia.authorization.model.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class AuthorizationResponseDto {

    private String token;

}
