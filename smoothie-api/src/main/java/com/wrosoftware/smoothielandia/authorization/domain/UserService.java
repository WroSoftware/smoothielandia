package com.wrosoftware.smoothielandia.authorization.domain;

import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationResponseDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.RegisterDto;
import com.wrosoftware.smoothielandia.authorization.model.entity.User;
import lombok.AllArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.function.Function;

import static java.util.Optional.ofNullable;

class UserService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final Function<User, String> generateTokenFunction;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, Function<User, String> generateTokenFunction) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.generateTokenFunction = generateTokenFunction;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = Mappers.getMapper(UserMapper.class);
    }

    public AuthorizationResponseDto login(AuthorizationDto dto) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(dto.getLogin(), dto.getPassword());
        authenticationManager.authenticate(authenticationToken);
        return userRepository.findByLogin(dto.getLogin())
                .map(generateTokenFunction::apply)
                .map(AuthorizationResponseDto::new)
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
    }

    public AuthorizationResponseDto register(RegisterDto dto) {
        return ofNullable(dto)
                .map(regDto -> userMapper.mapToUser(regDto, passwordEncoder.encode(regDto.getPassword())))
                .map(userRepository::save)
                .map(user -> AuthorizationDto.builder()
                        .login(dto.getLogin())
                        .password(dto.getPassword())
                        .build())
                .map(this::login)
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
    }
}
