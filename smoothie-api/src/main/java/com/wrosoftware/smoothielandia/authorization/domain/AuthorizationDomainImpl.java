package com.wrosoftware.smoothielandia.authorization.domain;

import com.wrosoftware.smoothielandia.authorization.AuthorizationDomain;
import com.wrosoftware.smoothielandia.authorization.TokenDomain;
import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.AuthorizationResponseDto;
import com.wrosoftware.smoothielandia.authorization.model.dto.RegisterDto;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
class AuthorizationDomainImpl implements AuthorizationDomain {

    private final UserService userService;

    public AuthorizationDomainImpl(UserRepository userRepository, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, TokenDomain tokenDomain) {
        this.userService = new UserService(userRepository, authenticationManager, passwordEncoder, tokenDomain::generateToken);
    }

    @Override
    public AuthorizationResponseDto login(AuthorizationDto dto) {
        return userService.login(dto);
    }

    @Override
    public AuthorizationResponseDto register(RegisterDto dto) {
        return userService.register(dto);
    }
}
