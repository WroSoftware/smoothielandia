package com.wrosoftware.smoothielandia.authorization;

import com.wrosoftware.smoothielandia.authorization.model.entity.User;

public interface TokenDomain {

    String extractUserName(String token);
    String generateToken(User userDetails);
    boolean isTokenExpired(String token);

}
