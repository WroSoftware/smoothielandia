package com.wrosoftware.smoothielandia.authorization.model.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AuthorizationDto {

    private String login;
    private String password;

}
