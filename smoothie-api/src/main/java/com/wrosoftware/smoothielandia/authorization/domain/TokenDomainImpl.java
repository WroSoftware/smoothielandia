package com.wrosoftware.smoothielandia.authorization.domain;

import com.wrosoftware.smoothielandia.authorization.TokenDomain;
import com.wrosoftware.smoothielandia.authorization.model.entity.Role;
import com.wrosoftware.smoothielandia.authorization.model.entity.User;
import com.wrosoftware.smoothielandia.common.UserContext;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
class TokenDomainImpl implements TokenDomain {

    private static final long EXPIRATION_TIME = 1000 * 60 * 30;
    private final String jwtSigningKey;

    public TokenDomainImpl(@Value("${credentials.jwt_key}") String jwtSigningKey) {
        this.jwtSigningKey = jwtSigningKey;
    }

    @Override
    public String extractUserName(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    @Override
    public String generateToken(User userDetails) {
        return Jwts.builder()
                .setClaims(prepareClaims(userDetails))
                .signWith(getSigningKey(), SignatureAlgorithm.HS512)
                .compact();
    }

    private Map<String, Object> prepareClaims(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", user.getLogin());
        claims.put("iat", new Date(System.currentTimeMillis()));
        claims.put("exp", new Date(System.currentTimeMillis() + EXPIRATION_TIME));
        claims.put("role", user.getRole().name());
        return claims;
    }

    @Override
    public boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public Role extractRole() {
        String role = extractAllClaims(UserContext.getJwtToken())
                .get("role", String.class);
        return Role.valueOf(role);
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolvers) {
        final Claims claims = extractAllClaims(token);
        return claimsResolvers.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(jwtSigningKey);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
