package com.wrosoftware.smoothielandia.authorization.model.entity;

import com.wrosoftware.smoothielandia.common.model.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

import static jakarta.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "operator", uniqueConstraints = @UniqueConstraint(columnNames = "login"))
@SequenceGenerator(name = "seq_gen", sequenceName = "operator_seq_gen", allocationSize = 1, initialValue = 1)
public class User extends BaseEntity implements UserDetails {

    private String login;
    private String password;
    @Enumerated(STRING)
    private Role role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
