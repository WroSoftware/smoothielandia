package com.wrosoftware.smoothielandia._configuration;

import com.wrosoftware.smoothielandia.authorization.AuthorizationDomain;
import com.wrosoftware.smoothielandia.authorization.TokenDomain;
import com.wrosoftware.smoothielandia.authorization.domain.UserDetailsService;
import com.wrosoftware.smoothielandia.common.UserContext;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Component
@RequiredArgsConstructor
class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final String AUTHORIZATION_KEY = "Authorization";
    private static final String BAERER_KEY = "Bearer ";
    private final TokenDomain tokenDomain;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain) throws ServletException, IOException {
        ofNullable(request.getHeader(AUTHORIZATION_KEY))
                .filter(this::validateToken)
                .map(this::setUpTokenOnUserContext)
                .ifPresent(token -> reloadAuthenticationContext(request));
        filterChain.doFilter(request, response);
    }

    private boolean validateToken(String header) {
        String token = header.substring(BAERER_KEY.length());
        return isAuthorizationHeader(header) && !tokenDomain.isTokenExpired(token);
    }

    private String setUpTokenOnUserContext(String header) {
        String token = header.substring(BAERER_KEY.length());
        UserContext.setJwtToken(token);
        return token;
    }

    private void reloadAuthenticationContext(HttpServletRequest request) {
        String userEmail = tokenDomain.extractUserName(UserContext.getJwtToken());
        if (isNotEmpty(userEmail) && isNull(SecurityContextHolder.getContext().getAuthentication())) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(userEmail);
            SecurityContext context = SecurityContextHolder.createEmptyContext();
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            context.setAuthentication(authToken);
            SecurityContextHolder.setContext(context);
        }
    }

    private boolean isAuthorizationHeader(String authHeader) {
        return isNotEmpty(authHeader) && StringUtils.startsWith(authHeader, BAERER_KEY);
    }

}
