package com.wrosoftware.smoothielandia.common;

public class UserContext {

    private static ThreadLocal<String> currentUser = new ThreadLocal<String>();

    public static void setJwtToken(String jwt) {
        currentUser.set(jwt);
    }

    public static String getJwtToken() {
        return currentUser.get();
    }

    public static void clear() {
        currentUser.remove();
    }


}
