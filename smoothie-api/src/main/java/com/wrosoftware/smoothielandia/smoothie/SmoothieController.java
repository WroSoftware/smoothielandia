package com.wrosoftware.smoothielandia.smoothie;

import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDetailsDto;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/smoothies")
@RequiredArgsConstructor
class SmoothieController {

    private final SmoothieDomain smoothieDomain;

    @GetMapping
    public List<SmoothieDto> getAllSmoothies() {
        return smoothieDomain.getAllSmoothies();
    }

    @GetMapping("/{id}")
    public SmoothieDetailsDto getDetails(@PathVariable("id") Long smoothieId) {
        return smoothieDomain.getDetails(smoothieId);
    }

    @PostMapping
    public SmoothieDetailsDto saveNewSmoothie(@RequestBody SmoothieDetailsDto dto) {
        return smoothieDomain.saveSmoothie(dto);
    }

    @PutMapping
    public void updateSmoothie(@RequestBody SmoothieDetailsDto dto) {
        smoothieDomain.saveSmoothie(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteSmoothie(@PathVariable("id") Long smoothieId) {
        smoothieDomain.deleteSmoothie(smoothieId);
    }
}
