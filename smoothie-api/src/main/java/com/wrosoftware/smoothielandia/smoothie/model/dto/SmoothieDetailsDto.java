package com.wrosoftware.smoothielandia.smoothie.model.dto;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
@Builder
@ToString
public class SmoothieDetailsDto {

    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private String currency;
    private BigDecimal portionSize;
    private BigDecimal calories;
    private List<NutritionDto> nutritions;
}
