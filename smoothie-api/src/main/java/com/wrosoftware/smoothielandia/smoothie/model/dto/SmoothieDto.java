package com.wrosoftware.smoothielandia.smoothie.model.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class SmoothieDto {

    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private String currency;

}
