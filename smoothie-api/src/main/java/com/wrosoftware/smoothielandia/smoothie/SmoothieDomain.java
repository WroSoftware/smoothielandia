package com.wrosoftware.smoothielandia.smoothie;

import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDetailsDto;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;

import java.util.List;

public interface SmoothieDomain {

    List<SmoothieDto> getAllSmoothies();

    SmoothieDetailsDto getDetails(Long smoothieId);

    SmoothieDetailsDto saveSmoothie(SmoothieDetailsDto dto);

    void deleteSmoothie(Long smoothieId);

    SmoothieDto getSmoothieBasic(Long smoothieId);
}
