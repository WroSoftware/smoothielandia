package com.wrosoftware.smoothielandia.smoothie.domain;


import com.wrosoftware.smoothielandia.smoothie.model.entity.Smoothie;
import org.springframework.data.jpa.repository.JpaRepository;

interface SmoothieRepository extends JpaRepository<Smoothie, Long> {

}
