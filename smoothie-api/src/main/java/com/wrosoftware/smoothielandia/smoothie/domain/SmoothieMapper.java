package com.wrosoftware.smoothielandia.smoothie.domain;

import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDetailsDto;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;
import com.wrosoftware.smoothielandia.smoothie.model.entity.Smoothie;
import org.mapstruct.Mapper;

@Mapper(uses = NutritionMapper.class)
interface SmoothieMapper {

    SmoothieDto mapToSmoothieDto(Smoothie smoothie);

    SmoothieDetailsDto mapToSmoothieDetailsDto(Smoothie smoothie);

    Smoothie mapToEntity(SmoothieDetailsDto smoothieDetailsDto);
}
