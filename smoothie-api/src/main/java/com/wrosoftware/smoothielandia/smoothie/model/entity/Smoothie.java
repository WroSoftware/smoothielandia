package com.wrosoftware.smoothielandia.smoothie.model.entity;

import com.wrosoftware.smoothielandia.common.model.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import java.math.BigDecimal;
import java.util.List;

import static jakarta.persistence.CascadeType.ALL;
import static org.hibernate.type.SqlTypes.LONGVARCHAR;

@Data
@Entity
@SequenceGenerator(name = "seq_gen", sequenceName = "smoothie_seq_gen", allocationSize = 1, initialValue = 1)
public class Smoothie extends BaseEntity {

    private String name;
    @JdbcTypeCode(LONGVARCHAR)
    private String description;
    private BigDecimal price;
    @Column(length = 3)
    private String currency;
    private BigDecimal portionSize;
    private BigDecimal calories;

    @OneToMany(cascade = ALL, orphanRemoval = true)
    @JoinColumn(name = "smoothie_id")
    private List<Nutrition> nutritions;

}
