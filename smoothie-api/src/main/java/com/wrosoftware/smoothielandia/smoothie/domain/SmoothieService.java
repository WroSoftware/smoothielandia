package com.wrosoftware.smoothielandia.smoothie.domain;

import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDetailsDto;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;
import org.mapstruct.factory.Mappers;

import java.util.List;

import static java.util.Optional.ofNullable;

class SmoothieService {

    private final SmoothieRepository smoothieRepository;
    private final SmoothieMapper smoothieMapper;

    public SmoothieService(SmoothieRepository smoothieRepository) {
        this.smoothieRepository = smoothieRepository;
        this.smoothieMapper = Mappers.getMapper(SmoothieMapper.class);
    }

    public List<SmoothieDto> getAllSmoothies() {
        return smoothieRepository.findAll()
                .stream()
                .map(smoothieMapper::mapToSmoothieDto)
                .toList();
    }

    public SmoothieDetailsDto getDetails(Long smoothieId) {
        return smoothieRepository.findById(smoothieId)
                .map(smoothieMapper::mapToSmoothieDetailsDto)
                .orElseThrow(() -> new RuntimeException("Cannot find smoothie with id "+smoothieId));
    }

    public SmoothieDetailsDto saveSmoothie(SmoothieDetailsDto dto) {
        return ofNullable(dto)
                .map(smoothieMapper::mapToEntity)
                .map(smoothieRepository::save)
                .map(smoothieMapper::mapToSmoothieDetailsDto)
                .orElseThrow(() -> new RuntimeException("Saving smoothie failed for request: "+dto));
    }

    public void deleteById(Long smoothieId) {
        smoothieRepository.deleteById(smoothieId);
        smoothieRepository.deleteById(smoothieId);
    }

    public SmoothieDto getSmoothieBasicInfo(Long smoothieId) {
        return smoothieRepository.findById(smoothieId)
                .map(smoothieMapper::mapToSmoothieDto)
                .orElseThrow(() -> new RuntimeException("Cannot find smoothie with id "+smoothieId));
    }
}
