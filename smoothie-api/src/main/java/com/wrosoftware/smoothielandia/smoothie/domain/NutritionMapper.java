package com.wrosoftware.smoothielandia.smoothie.domain;

import com.wrosoftware.smoothielandia.smoothie.model.dto.NutritionDto;
import com.wrosoftware.smoothielandia.smoothie.model.entity.Nutrition;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
interface NutritionMapper {

    @Mapping(target = "masss", source = "mass")
    NutritionDto mapToDto(Nutrition smoothie);

    @Mapping(target = "mass", source = "masss")
    Nutrition mapToEntity(NutritionDto dto);

}
