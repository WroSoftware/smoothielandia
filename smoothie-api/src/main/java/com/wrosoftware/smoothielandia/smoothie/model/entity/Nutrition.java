package com.wrosoftware.smoothielandia.smoothie.model.entity;

import com.wrosoftware.smoothielandia.common.model.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
@SequenceGenerator(name = "seq_gen", sequenceName = "nutrition_seq_gen", allocationSize = 1, initialValue = 1)
public class Nutrition extends BaseEntity {

    private String name;
    private BigDecimal mass;
    private BigDecimal percentage;

}
