package com.wrosoftware.smoothielandia.smoothie.model.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class NutritionDto {

    private Long id;
    private String name;
    private BigDecimal masss;
    private BigDecimal percentage;
}
