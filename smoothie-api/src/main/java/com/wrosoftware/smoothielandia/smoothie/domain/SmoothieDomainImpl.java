package com.wrosoftware.smoothielandia.smoothie.domain;

import com.wrosoftware.smoothielandia.smoothie.SmoothieDomain;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDetailsDto;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
class SmoothieDomainImpl implements SmoothieDomain {

    private final SmoothieService smoothieService;

    public SmoothieDomainImpl(SmoothieRepository smoothieRepository) {
        this.smoothieService = new SmoothieService(smoothieRepository);
    }

    @Override
    public List<SmoothieDto> getAllSmoothies() {
        return smoothieService.getAllSmoothies();
    }

    @Override
    public SmoothieDetailsDto getDetails(Long smoothieId) {
        return smoothieService.getDetails(smoothieId);
    }

    @Override
    public SmoothieDetailsDto saveSmoothie(SmoothieDetailsDto dto) {
        return smoothieService.saveSmoothie(dto);
    }

    @Override
    public void deleteSmoothie(Long smoothieId) {
        smoothieService.deleteById(smoothieId);
    }

    @Override
    public SmoothieDto getSmoothieBasic(Long smoothieId) {
        return smoothieService.getSmoothieBasicInfo(smoothieId);
    }

}
