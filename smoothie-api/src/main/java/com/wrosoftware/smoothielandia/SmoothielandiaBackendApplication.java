package com.wrosoftware.smoothielandia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmoothielandiaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmoothielandiaBackendApplication.class, args);
	}

}
