package com.wrosoftware.smoothielandia.order.domain;

import com.wrosoftware.smoothielandia.order.model.dto.OrderDto;
import com.wrosoftware.smoothielandia.order.model.entity.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = OrderItemMapper.class)
interface OrderMapper {

    OrderDto mapToDto(Order order);

    Order mapToEntity(OrderDto dto);

}
