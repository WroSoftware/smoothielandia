package com.wrosoftware.smoothielandia.order.model.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
@Builder
public class OrderDto {

    private Long id;
    private BigDecimal totalPrice;
    private List<OrderItemDto> items;

}
