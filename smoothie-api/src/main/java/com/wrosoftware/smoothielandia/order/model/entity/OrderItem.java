package com.wrosoftware.smoothielandia.order.model.entity;

import com.wrosoftware.smoothielandia.common.model.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;

@Data
@Entity
@SequenceGenerator(name = "seq_gen", sequenceName = "order_item_seq_gen", allocationSize = 1, initialValue = 1)
public class OrderItem extends BaseEntity {

    private Long smoothieId;
    private Integer quantity;

}
