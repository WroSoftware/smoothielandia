package com.wrosoftware.smoothielandia.order.domain;

import com.wrosoftware.smoothielandia.order.OrderDomain;
import com.wrosoftware.smoothielandia.order.model.dto.OrderDto;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
class OrderDomainImpl implements OrderDomain {

    private OrderService orderService;

    public OrderDomainImpl(OrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderService = new OrderService(orderRepository, orderMapper);
    }

    @Override
    public OrderDto saveOrder(OrderDto dto) {
        return orderService.saveOrder(dto);
    }

    @Override
    public OrderDto getOrderDetails(Long orderId) {
        return orderService.getOrderDetails(orderId);
    }
}
