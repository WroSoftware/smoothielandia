package com.wrosoftware.smoothielandia.order.model.dto;

import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class OrderItemDto {

    private SmoothieDto smoothie;
    private Integer quantity;

}
