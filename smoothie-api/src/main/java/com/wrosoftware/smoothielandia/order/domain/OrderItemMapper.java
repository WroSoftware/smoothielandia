package com.wrosoftware.smoothielandia.order.domain;

import com.wrosoftware.smoothielandia.order.model.dto.OrderItemDto;
import com.wrosoftware.smoothielandia.order.model.entity.OrderItem;
import com.wrosoftware.smoothielandia.smoothie.SmoothieDomain;
import com.wrosoftware.smoothielandia.smoothie.model.dto.SmoothieDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
abstract class OrderItemMapper {

    @Autowired
    private SmoothieDomain smoothieDomain;

    @Mapping(target = "smoothieId", source = "smoothie.id")
    abstract OrderItem mapToEntity(OrderItemDto dto);

    @Mapping(target = "smoothie", source = "smoothieId", qualifiedByName = "findSmoothie")
    abstract OrderItemDto mapToDto(OrderItem item);

    @Named("findSmoothie")
    protected SmoothieDto findSmoothie(Long smoothieId) {
        return smoothieDomain.getSmoothieBasic(smoothieId);
    }
}
