package com.wrosoftware.smoothielandia.order.domain;

import com.wrosoftware.smoothielandia.order.model.dto.OrderDto;
import com.wrosoftware.smoothielandia.order.model.entity.Order;
import org.mapstruct.factory.Mappers;

import static java.util.Optional.ofNullable;

class OrderService {

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    public OrderDto saveOrder(OrderDto dto) {
        return ofNullable(dto)
                .map(orderMapper::mapToEntity)
                .map(orderRepository::save)
                .map(order -> prepareResponseDto(dto, order))
                .orElseThrow(() -> new RuntimeException("Saving order failed"));
    }

    private OrderDto prepareResponseDto(OrderDto dto, Order order) {
        return OrderDto.builder()
                .id(order.getId())
                .items(dto.getItems())
                .totalPrice(dto.getTotalPrice())
                .build();
    }

    public OrderDto getOrderDetails(Long orderId) {
        return orderRepository.findById(orderId)
                .map(orderMapper::mapToDto)
                .orElseThrow(() -> new RuntimeException("Cannot find order with id "+orderId));
    }
}
