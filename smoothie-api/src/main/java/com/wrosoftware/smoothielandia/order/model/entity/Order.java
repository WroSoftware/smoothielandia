package com.wrosoftware.smoothielandia.order.model.entity;

import com.wrosoftware.smoothielandia.common.model.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

import static jakarta.persistence.CascadeType.ALL;

@Data
@Entity
@Table(name = "smoothie_order")
@SequenceGenerator(name = "seq_gen", sequenceName = "order_seq_gen", allocationSize = 1, initialValue = 1)
public class Order extends BaseEntity {

    private BigDecimal totalPrice;
    @OneToMany(cascade = ALL, orphanRemoval = true)
    @JoinColumn(name = "order_id")
    private List<OrderItem> items;

}
