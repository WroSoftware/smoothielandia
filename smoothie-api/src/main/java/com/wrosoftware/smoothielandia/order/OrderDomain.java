package com.wrosoftware.smoothielandia.order;

import com.wrosoftware.smoothielandia.order.model.dto.OrderDto;

public interface OrderDomain {

    OrderDto saveOrder(OrderDto dto);

    OrderDto getOrderDetails(Long orderId);
}
