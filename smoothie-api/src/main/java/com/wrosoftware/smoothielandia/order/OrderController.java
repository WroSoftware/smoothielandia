package com.wrosoftware.smoothielandia.order;

import com.wrosoftware.smoothielandia.order.model.dto.OrderDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
@AllArgsConstructor
class OrderController {

    private OrderDomain orderDomain;

    @PostMapping
    public OrderDto saveNewOrder(@RequestBody OrderDto dto) {
        return orderDomain.saveOrder(dto);
    }

    @GetMapping("/{id}")
    public OrderDto getOrderDetails(@PathVariable("id") Long orderId) {
        return orderDomain.getOrderDetails(orderId);
    }
}
