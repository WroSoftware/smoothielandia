package com.wrosoftware.smoothielandia.order.domain;

import com.wrosoftware.smoothielandia.order.model.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

interface OrderRepository extends JpaRepository<Order, Long> {
}
